################################################################################
###    data_generator.py                                                     ###
###    Part of multilayer perceptron neural network classifier with Qt GUI.  ###
###    Written for summer semester 20/21 VSC class at VUT Brno IACS.         ###
###    2021, Andrej Pillar <192235@vutbr.cz>                                 ###
###    Licensed under WTFPL feel free to steal                               ###
################################################################################

import random
from multiprocessing import Pool
import numpy as np

class DatasetGen(object):
    """
    Contains dataset generation functions.
    :param int thread_count: Number of threads to use, 0 to disable multithreading
    :param bool use_system_rand: Flag to choose system randomness source
    """
    def __init__(self, thread_count, use_system_rand=True):
        self.use_system_rand = use_system_rand
        self.thread_count = thread_count
        self.threaded = True if thread_count > 0 else False

    # Specified limits
    # (x1, x2)  ∈ <−4; 2> ✕ <2; 5>

    def _random_fill(self, point):
        """
        Fills list or array with two random values.
        :param ndarray point: Array to fill
        """
        point[0] = random.SystemRandom().uniform(-4.00, 2.00)
        point[1] = random.SystemRandom().uniform(2.00, 5.00)
        return point

    def new(self, size, only_pts=False):
        """
        Generates a specified number of points and classifications.
        :param int size: Number of points to generate.
        :param bool only_pts: Flag to disable class generation, default False.
        :param bool threaded: Toggle multithreaded processing, default True.
        :return: Numpy arrays of points and classifications
        :rtype: ndarray
        """
        points = np.zeros((size, 2))
        classes = np.zeros(size, int)
        if self.use_system_rand:
            if self.threaded:
                pool = Pool(self.thread_count)
                points = np.array(pool.map(self._random_fill, points))
                pool.close()
                pool.join()
            else:
                for point in points:
                    point[0] = random.SystemRandom().uniform(-4.00, 2.00)
                    point[1] = random.SystemRandom().uniform(2.00, 5.00)
        else:
            for point in points:
                point[0] = random.uniform(-4.00, 2.00)
                point[1] = random.uniform(2.00, 5.00)

        if not only_pts:
            classes = self.classify(points)
            return points, classes
        else:
            return points

    def classify(self, points):
        """
        Returns an array of classifications corresponding to points
        :param ndarray points: Array of points to classify
        :return: Array of classifications
        :rtype: ndarray
        """
        if self.threaded:
            pool = Pool(self.thread_count)
            classes = np.array(pool.map(self._is_inside, points))
            pool.close()
            pool.join()
        else:
            size = len(points)
            classes = np.zeros(size, int)
            for j in range(size):
                classes[j] = self._is_inside(points[j])
        return classes

    def _is_inside(self, point):
        """
        Returns classification of a given point.
        :param ndarray point: Point to classify.
        :return: Classification
        :rtype: int
        """
        if 0.4444444*(point[0]+2)**2 + 2.3668639*(point[1]-3)**2 < 1:
            return 1
        else:
            return 0