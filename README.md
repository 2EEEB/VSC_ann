# VSC_ann

Artificial neural network classifier project for VSC class.
Based on scikit-learn multilayer perceptron library.
Contains basic Qt UI.
Includes [poetry](https://python-poetry.org/) project file, easily set up by runnning `poetry install`.
The program is launched from ann_classifier.py
