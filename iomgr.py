################################################################################
###    iomgr.py                                                              ###
###    Part of multilayer perceptron neural network classifier with Qt GUI.  ###
###    Written for summer semester 20/21 VSC class at VUT Brno IACS.         ###
###    2021, Andrej Pillar <192235@vutbr.cz>                                 ###
###    Licensed under WTFPL feel free to steal                               ###
################################################################################

import os
import csv
from multiprocessing import Pool

class IOManager(object):
    """
    File and stdout related functions
    :param int thread_count: Number of threads to use, 0 to disable multithreading
    """
    def __init__(self, thread_count):
        self.thread_count = thread_count
        self.threaded = True if thread_count > 0 else False

    def parse_csv(self, file, threaded=True):
        """
        Parses point list from a csv file.
        :param str file: Path to csv file
        :param bool threaded: Flag to choose multithreaded processing
        :return: list of lists of point coordinates
        :rtype: list
        """
        with open(file, "r", newline="") as infile:
            content = [line for line in infile if line.strip()]         #don't read empty lines
            parsed_content = csv.reader(content, delimiter=";")         #instantiate csv reader with semicolon as delimiter
            if self.threaded:
                pool = Pool(self.thread_count)
                points = pool.map(self._process_csv_row, parsed_content)    #process read rows in threaded fashion
                pool.close()
                pool.join()
            else:
                self.points = []
                for row in parsed_content:
                    points.append(self._process_csv_row(row))               #process read rows in a loop (very slow for large files)
        return points

    def _process_csv_row(self, csv_row):
        """
        Process list of parsed csv data.
        :param list csv_row: List of parsed csv columns
        :return: Processed row
        :rtype: list
        """
        if len(csv_row) != 2:
            csv_row = csv_row[:2]                                           #two columns are expected, drop anything beyond that
        try:
            csv_row[0] = float(csv_row[0].replace(",", "."))                #replace commas by periods and save as float
            csv_row[1] = float(csv_row[1].replace(",", "."))
        except Exception as e:
            print("Problems processing CSV: {}".format(e))
        return csv_row

    def print_results(self, points, cls_real):
        """
        Neatly prints given arrays to stdout.
        :param ndarray points: Array of point coords
        :param ndarray cls_real: Array of point classifications
        """
        print("{:>4} {:>5} {:>8}".format("x1", "x2", "Class"))
        for i in range(len(cls_real)):
            print("{:>6.3f};{:<6.3f}:{}".format(points[i][0], points[i][1], "Inside" if cls_real[i] == 1 else "Outside"))

    def dump_results(self, points, cls_real, fname):
        """
        Save given arrays to disk as semicolon delimited csv.
        :param ndarray points: Array of point coords
        :param ndarray cls_real: Array of point classifications
        :param str fname: Name for the resulting file
        """
        #should not occur but just in case
        assert (len(points) == len(cls_real)), "Number of points and classifications must match!"

        with open(fname, 'w') as outf:
            writer = csv.writer(outf, dialect='unix', delimiter=';')
            for i in range(len(cls_real)):
                writer.writerow(["{:.3f}".format(points[i][0]),"{:.3f}".format(points[i][1]), "Inside" if cls_real[i] == 1 else "Outside"])