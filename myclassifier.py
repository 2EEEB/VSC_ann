################################################################################
###    myclassifer.py                                                        ###
###    Part of multilayer perceptron neural network classifier with Qt GUI.  ###
###    Written for summer semester 20/21 VSC class at VUT Brno IACS.         ###
###    2021, Andrej Pillar <192235@vutbr.cz>                                 ###
###    Licensed under WTFPL feel free to steal                               ###
################################################################################

import os
import joblib
from sklearn.neural_network import MLPClassifier

class MyClassifier(object):
    """Classifier models and training functions"""
    def __init__(self):
        self.model = self._load_trained("default.pkl")

    def _load_trained(self, path):
        """
        Loads pickled model.
        :param str path: Path to pickle
        :return: Loaded trained MLPClassifier model
        :rtype: MLPClassifier instance
        :raises Exception: if invalid path is entered
        """
        if os.path.exists(path):
            return joblib.load(path)
        else:
            print("Invalid path!")

    def load_and_set(self, fname):
        """
        Helper function to load pickled model and set it as active
        :param str fname: Path to pickle
        """
        self.model = self._load_trained(fname)

    def train_new_parameterized(self, trn_pts, trn_cls, layers, act, slv, max_iter):
        """
        Runs training of new MLPClassifier with selected parameters.
        :param ndarray trn_pts: Array of training points
        :param ndarray trn_cls: Point classifications
        :param tuple layers: Tuple of hidden layer sizes
        :param str act: Activation function, one of 'identity', 'logistic', 'tanh', 'relu'
        :param str slv: Solver algorithm, one of 'lbfgs', 'sgd', 'adam'
        :param int max_iter: Maximum number of iterations to run the training for
        """
        self.model = MLPClassifier(hidden_layer_sizes=layers, activation=act, solver=slv, max_iter=max_iter).fit(trn_pts, trn_cls)

    def train_new(self, trn_pts, trn_cls):
        """
        Trains a classifier model with preset parameters.
        :param ndarray trn_pts: Array of training points
        :param ndarray trn_cls: Training point classifications
        """
        self.model = MLPClassifier(hidden_layer_sizes=(50,50), activation='relu', solver='lbfgs', max_iter=1000, verbose=False).fit(trn_pts, trn_cls)

    def save_current_model(self, fname):
        """
        Saves currently selected model to disk as joblib pickle.
        :param str fname: Path to save to
        :raises Exception: if there are any problems while saving the model
        """
        try:
            joblib.dump(self.model, fname)
        except Exception as e:
            print("An error occurred when trying to save a model: {}".format(e))