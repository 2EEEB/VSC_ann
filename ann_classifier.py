################################################################################
###    ann_classifer.py                                                      ###
###    Part of multilayer perceptron neural network classifier with Qt GUI.  ###
###    Written for summer semester 20/21 VSC class at VUT Brno IACS.         ###
###    2021, Andrej Pillar <192235@vutbr.cz>                                 ###
###    Licensed under WTFPL feel free to steal                               ###
################################################################################

import sys
import os
import time
import datetime
import random

import numpy as np
from PyQt5 import uic
import PyQt5.QtWidgets as qtw
import PyQt5.QtCore as qtc
import PyQt5.QtGui as qtg

import data_generator
import painter
import myclassifier
import iomgr

THREAD_COUNT=8

class WorkerSingals(qtc.QObject):
    """Signals for multithreaded workers"""
    finished = qtc.pyqtSignal()
    result = qtc.pyqtSignal(object)

class Worker(qtc.QRunnable):
    """
    Worker class for launching functions in threads.
    :param callable fn: Function to execute
    :param args: Arguments to pass to fn
    :param kwargs: Keyword args to pass to fn
    """
    def __init__(self, fn, *args, **kwargs):
        super(Worker, self).__init__()
        self.fn = fn
        self.args = args
        self.kwargs = kwargs
        self.signals = WorkerSingals()

    @qtc.pyqtSlot()
    def run(self):
        res = self.fn(*self.args, **self.kwargs)
        self.signals.result.emit(res)
        self.signals.finished.emit()

class MainWindow(qtw.QMainWindow):
    def __init__(self):
        super().__init__()
        uic.loadUi('main_layout.ui', self)
        #instantiate components and aliases
        self.threadpool = qtc.QThreadPool()
        self.train_dlg = TrainingWindow(self)
        self.datagen = data_generator.DatasetGen(THREAD_COUNT, use_system_rand=True)
        self.clfr = myclassifier.MyClassifier()
        self.io = iomgr.IOManager(THREAD_COUNT)
        self.ax = self.canvas.figure.add_subplot()
        self.paint = painter.Painter(self.canvas, self.ax)
        #bind events
        self.actionExit.triggered.connect(qtw.qApp.quit)
        self.actionOpen_CSV.triggered.connect(self._handle_csvopen_dialog)
        self.actionTrain_new.triggered.connect(self._handle_training_dialog)
        self.actionLoad.triggered.connect(self._handle_model_loading)
        self.canvas.mpl_connect("button_press_event", self._handle_canvas_lclick)
        #init dataset and view to empty
        self.paint.reset_canvas()
        self.to_class = np.array([[]])
        #disable run button
        self.cls_button.setEnabled(False)

    #UI button handlers
    def handle_training(self):
        """Runs training with default params on random dataset"""
        worker = Worker(self._run_random_training)
        self.threadpool.start(worker)
        worker.signals.result.connect(self._training_done)

    def handle_classification(self, clicked=False, to_cls=None, cls_arg=None, training=None):
        """Runs classification on loaded data."""
        self.inform("Classifying...", timeout=0)
        data = to_cls if to_cls is not None else self.to_class
        worker = Worker(self._run_classification, data, cls_arg, training)
        self.threadpool.start(worker)
        worker.signals.result.connect(self._classification_done)

    def randomize_canvas(self):
        """Generate and visualize a random dataset."""
        self.paint.reset_canvas()
        self.to_class = self.datagen.new(random.randint(10, 1000), only_pts=True)
        self.cls_button.setEnabled(True)
        self._visualize_dataset()
        self.inform("Generated {} points".format(len(self.to_class)), timeout=3000)      #inform user of how many points were generated

    def clr_canvas_and_data(self):
        """Remove everything from canvas and clear saved dataset."""
        self.cls_button.setEnabled(False)                                                       #disable launching of classifier
        self.paint.reset_canvas()                                                               #clear everything from canvas
        self.to_class = np.array([[]])                                                          #reinitialize dataset to empty

    #menubar action and event handlers
    def _handle_training_dialog(self):
        """Handle opening of advanced training window."""
        self.train_dlg.show_dlg()

    def _handle_model_loading(self):
        """Handle model loading dialog."""
        fname, _ = qtw.QFileDialog.getOpenFileName(self,"Load pickled model", os.getcwd(), "Joblib pickle (*.pkl *.joblib);;All Files (*)")
        if fname:
            self.clfr.load_and_set(fname)

    def _handle_csvopen_dialog(self):
        """Handle dataset loading from csv. Responsible for correct typing and sizing."""
        fname, _ = qtw.QFileDialog.getOpenFileName(self,"Open CSV with values", os.getcwd(), "Comma Separated Values (*.csv);;All Files (*)")
        if fname:
            self.paint.reset_canvas()
            parsed = np.array(self.io.parse_csv(fname, threaded=True))                          #convert list of points into numpy array
            self.to_class = np.clip(parsed, [-4,2], [2,5])                                      #limit points to given interval limits
            self.inform("Imported {} points".format(len(self.to_class)), timeout=3000)          #inform user of dataset size
            self._visualize_dataset()                                                           #show loaded dataset on canvas
            self.cls_button.setEnabled(True)                                                    #enable run button

    def _handle_canvas_lclick(self, e):
        """Handles visual point addition."""
        # lmb is represented by a MouseButton value of 1
        if e.button == 1:
            x1, x2 = e.xdata, e.ydata
            if x1 and x2:
                if x1 > -4 and x1 < 2 and x2 > 2 and x2 < 5:                                    #only append to dataset of witin interval boundaries
                    self.ax.plot(x1, x2, 'mx')
                    #extending nested numpy arrays is anything but straightforward
                    self.to_class = np.append(self.to_class, [[x1, x2]], axis=1) if len(self.to_class[0])==0 else np.append(self.to_class, [[x1, x2]], axis=0)
                    self.cls_button.setEnabled(True)                                            #we have data so we can classify it
                else:
                    self.ax.plot(x1, x2, 'rx')                                                  #red color indicates point out of bounds
                self.canvas.draw()                                                              #draw after every click

    #Threaded work callbacks
    def _training_done(self, results):
        """
        Informs user and executes visualisation of newly trained model
        :param tuple results: Tuple containing training sets and model score
        """
        train_X, train_y, demo_X, demo_y, score = results              #unpack result tuple
        self.train_dlg.btn_save.setEnabled(True)                                                        #enable save button
        self.train_dlg.textEdit.append("Trained model on {} points, tested on {}, precision is {:.2f}.".format(len(train_X),len(demo_X),score))
        self.to_class = demo_X
        self.handle_classification(to_cls=self.to_class, cls_arg=demo_y, training=(train_X, train_y))
        self.cls_button.setEnabled(True)

    def _classification_done(self, results):
        """Handles classification results."""
        points, clfd, cls_diff, xx, yy, Z, score, training = results   #unpack result tuple
        self.inform("Classified {} points".format(len(points)), timeout=3000)
        self.paint.plot_results(points, clfd, (xx,yy,Z), cls_diff, score=score, training=training) #visualize results on canvas
        self.io.print_results(points, clfd)                  #dump classification results to stdout

        if self.chb_dump.isChecked():
            fname = self._get_csv_fname()                              #ask for filename if the user chooses to export
            if fname:
                self.io.dump_results(points, clfd, fname)    #export points and classifications as csv

    #support functions
    def _visualize_dataset(self):
        """Draws loaded dataset on canvas."""
        self.paint.plot_points(self.to_class)

    def _prepare_point_mesh(self, points):
        """Prepares mesh of points, later used for decision boundary approximation"""
        x_min, x_max = points[:, 0].min() - .3, points[:, 0].max() + .3
        y_min, y_max = points[:, 1].min() - .3, points[:, 1].max() + .3
        xx, yy = np.meshgrid(np.arange(x_min, x_max, .2), np.arange(y_min, y_max, .2))
        return xx, yy

    def _item_diff(self, itm1, itm2):
        """
        Marks identical values 0 and differing as 1.
        :param itm1: First item to compare
        :param itm2: Second item to compare
        """
        if itm1 == itm2:
            return 0
        else:
            return 1

    def _get_csv_fname(self):
        """Handles save dialog when saving classification resutls."""
        defaultname = "classification_{}.csv".format(datetime.datetime.fromtimestamp(int(time.time())).isoformat()).replace(":","-")
        fname, _ = qtw.QFileDialog.getSaveFileName(self,"Save results as CSV", os.getcwd()+"/"+defaultname, "Comma Separated Values (*.csv);;All Files (*.*)")
        #hack for sway wm which does not append extension automatically
        if fname:
            path = fname.split("/")
            path[-1] = "".join(i for i in path[-1] if i not in r'\/:*?"<>|')
            fname = os.path.join("/",*path)
            if os.getenv('XDG_CURRENT_DESKTOP') == "sway" and not ".csv" in fname:
                fname += ".csv"
        return fname

    def inform(self, message, timeout=0):
        self.statusbar.showMessage(message, timeout)

    #actual work performing functions
    def _run_random_training(self):
        """Launches classifier model training on a dataset of random length."""
        train_X, train_y = self.datagen.new(random.randint(300,800))                            #generate random training dataset
        demo_X, demo_y = self.datagen.new(random.randint(10,1000))                              #generate random testing dataset
        self.clfr.train_new(train_X,train_y)                                                    #run training with default values
        score = self.clfr.model.score(demo_X, demo_y)

        return (train_X, train_y, demo_X, demo_y, score)

    def _run_classification(self, points, classes=None, training=None):
        """
        Executes classification of a set of points.
        :param ndarray points: Numpy array points to classify, expects array of two item arrays.
        :param ndarray classes: List or numpy array of reference classifications for every point.
        """
        #should not be needed since we disable the launch button, but just in case
        if len(points[0]) == 0:
            err_msg = qtw.QMessageBox()
            err_msg.setIcon(qtw.QMessageBox.Critical)
            err_msg.setText("Error")
            err_msg.setInformativeText("Nothing to classify!")
            err_msg.setWindowTitle("Error")
            err_msg.exec_()
            return

        if classes is not None:
            cls_act = classes                                                                   #store actual classifications if present
        else:
            cls_act = self.datagen.classify(points)

        clfd = self.clfr.model.predict(points)                                                  #model classifications
        score = self.clfr.model.score(points, cls_act)                                          #generate precision score
        cls_diff = list(map(self._item_diff,clfd,cls_act))                                      #find misclassifications
        xx,yy = self._prepare_point_mesh(points)                                                #prepare point mesh for decision boundary plot
        Z = self.clfr.model.predict_proba(np.c_[xx.ravel(), yy.ravel()])[:, 1]                  #estimate decision boundary
        Z = Z.reshape(xx.shape)

        return (points, clfd, cls_diff, xx, yy, Z, score, training)

class TrainingWindow(qtw.QDialog):
    def __init__(self, parent):
        super().__init__()
        self.parent = parent
        self.threadpool = qtc.QThreadPool()
        uic.loadUi('training_layout.ui', self)
        #init training options
        self.solvers = ["lbfgs", "sgd", "adam"]
        self.act_fn = ["identity", "logistic", "tanh", "relu"]
        #init input box rules
        layer_regex = qtc.QRegExp("^[0-9,]+(,[0-9]+)*$") #numbers and commas only
        layer_validator = qtg.QRegExpValidator(layer_regex)
        size_regex = qtc.QRegExp("^[0-9]*$") #numbers only
        size_validator = qtg.QRegExpValidator(size_regex)
        self.layer_edit.setValidator(layer_validator)
        self.datasize_edit.setValidator(size_validator)
        #disable save button since there is nothing to save yet
        self.btn_save.setEnabled(False)

    #UI button handlers
    def handle_training(self):
        """
        Handles instantiating of a training worker.
        """
        self._inform("Training new model...", 0)
        worker = Worker(self._run_training)
        self.threadpool.start(worker)
        worker.signals.result.connect(self._training_done)

    def handle_save(self):
        """Handles requests to save a loaded model."""
        self._save_model()

    #support functions
    def _inform(self, message, timeout=0):
        """
        Prints message to main window status bar.
        :param str message: Message to display
        :param int timout: Message timout in ms, 0 for no timeout
        """
        self.parent.inform(message, timeout)

    def _get_actfn(self):
        """Returns index of requested activation function"""
        if self.radio_ident.isChecked():
            return self.act_fn.index("identity")
        elif self.radio_logi.isChecked():
            return self.act_fn.index("logistic")
        elif self.radio_tanh.isChecked():
            return self.act_fn.index("tanh")
        elif self.radio_relu.isChecked():
            return self.act_fn.index("relu")
        else:
            return None

    def _get_solver(self):
        """Returns index of requested solver model"""
        if self.radio_lbfgs.isChecked():
            return self.solvers.index("lbfgs")
        elif self.radio_sgd.isChecked():
            return self.solvers.index("sgd")
        elif self.radio_adam.isChecked():
            return self.solvers.index("adam")
        else:
            return None

    def _make_int_if_exists(self, to_convert):
        """
        Converts string numbers to integers. In case of an empty string returns 1.
        :param to_convert: String to convert
        :return: Input string as integer or one
        :rtype: int
        """
        if to_convert != '':
            return int(to_convert)
        else:
            return 1

    def _get_layers(self):
        """Processes chosen layer count and sizes and returns as a tuple"""
        entry = self.layer_edit.text().strip(",")                  #read whatever is entered in editbox
        entry = map(self._make_int_if_exists, entry.split(","))   #strip leading/trailing commas, split and save as list of ints, replacing null values with ones
        layer_tup = tuple(entry)                        #create a tuple of layer sizes
        return layer_tup

    #actual work functions
    def _run_training(self):
        """Trains new classifier with chosen parameters"""
        ch_slv = self._get_solver()
        ch_afn = self._get_actfn()
        max_iter = self.slider_itercount.value()
        layers = self._get_layers()
        dset_size = int(self.datasize_edit.text()) if self.datasize_edit.text() != '' and self.datasize_edit.text() != '0' else 500

        train_X, train_y = self.parent.datagen.new(dset_size)
        demo_X, demo_y = self.parent.datagen.new(random.randint(500, 1500))

        self.parent.clfr.train_new_parameterized(train_X, train_y, layers, self.act_fn[ch_afn], self.solvers[ch_slv], max_iter)
        score = self.parent.clfr.model.score(demo_X, demo_y)

        return (train_X, train_y, demo_X, demo_y, score)

    def _save_model(self):
        """Handle save dialog for saving classifier model."""
        fname, _ = qtw.QFileDialog.getSaveFileName(self,"Save model", os.getcwd()+"/my_model.pkl", "Joblib pickle (*.pkl *.joblib);;All Files (*.*)")
        #hack for sway wm which does not append extension automatically
        if fname:
            path = fname.split("/")
            path[-1] = "".join(i for i in path[-1] if i not in r'\/:*?"<>|')
            fname = os.path.join("/",*path)
            if os.getenv('XDG_CURRENT_DESKTOP') == "sway" and not ".pkl" in fname:
                fname += ".pkl"
            self.parent.clfr.save_current_model(fname)

    #threaded work callback
    def _training_done(self, results):
        """
        Handle training results.
        :param tuple results: Results of the training function
        """
        train_X, train_y, demo_X, demo_y, score = results
        self._inform("New model trained.", timeout=3000)
        self.btn_save.setEnabled(True)
        self.textEdit.append("Trained model on {} points, tested on {}, precision is {:.2f}.".format(len(train_X),len(demo_X),score))
        self.parent.to_class = demo_X

        if self.chb_visu.isChecked():
            self.parent.handle_classification(to_cls=demo_X, cls_arg=demo_y, training=(train_X, train_y))

        self.parent.cls_button.setEnabled(True)

    #window activation
    def show_dlg(self):
        """Displays training options window."""
        self.parent.trn_button.setEnabled(False)      #disable quick train button in main window while dialog is open
        if not self.exec_():
            self.parent.trn_button.setEnabled(True)

if __name__ == "__main__":
    app = qtw.QApplication([])
    app.setStyle('qt5ct-style')
    window = MainWindow()
    window.show()
    app.exec()
