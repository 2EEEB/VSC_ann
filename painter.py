import matplotlib
import numpy as np

class Painter(object):
    """Plotting functions."""
    def __init__(self, canvas, subplot):
        self.canvas = canvas
        self.ax = subplot
        self.point_cmap = matplotlib.colors.ListedColormap(['#FF0000', '#00F0FF'])       #classification point cmap

    def reset_canvas(self, prob_mesh=None):
        """
        Removes everything from the plot, resets axes and redraws boundaries.
        :param tuple prob_mesh: Point mesh, used to properly scale axes to dataset.
        """
        self.ax.clear()
        # plot interval limits
        lims = [([-4,2], [2,2]),([2,2], [2,5]),([2,-4], [5,5]),([-4,-4], [5,2])]
        for pt in lims:
            self.ax.plot(pt[0], pt[1], color='lightgray', linestyle=(0,(5,10)), linewidth=0.7)

        self.plot_class_boundary()
        #label axes
        self.ax.set_xlabel("x1")
        self.ax.set_ylabel("x2")
        #set axis limits
        if not prob_mesh:
            self.ax.set_xlim(-4.3, 2.3)
            self.ax.set_ylim(1.7, 5.3)
        else:
            #used to set axis limits according to real dataset, providing more detail
            self.ax.set_xlim(prob_mesh[0].min(), prob_mesh[0].max())
            self.ax.set_ylim(prob_mesh[1].min(), prob_mesh[1].max())

        #minimize canvas whitespace
        self.canvas.figure.tight_layout()
        return self.canvas.draw()

    def plot_points(self, points):
        """
        Draw scatter plot of points marked by magenta Xs.
        :param ndarray points: Points to plot
        """
        self.ax.scatter(points[:,0], points[:,1], marker='x', color='m')
        return self.canvas.draw()

    def plot_class_boundary(self):
        """Plots class boundary ellipse."""
        x, y, a, b=-2, 3, 1.5, 0.65
        t = np.linspace(0, 2*np.pi, 100)
        self.ax.plot(x+a*np.cos(t), y+b*np.sin(t), ':', color='darkgrey', linewidth=1)

    def plot_training(self, points, cls_real):
        """
        Plots training dataset with square markers at half opacity
        :param ndarray points: Training points
        :param ndarray cls_real: Point classifications
        """
        #plot training dataset with smaller markers and at half opacity
        train_scatter = self.ax.scatter(points[:, 0], points[:, 1], c=cls_real, cmap=self.point_cmap, s=20, alpha=0.5, marker='s', edgecolors='lightgrey')
        legend_trn = self.ax.legend(*train_scatter.legend_elements(), loc="lower center", title="Training set")
        self.ax.add_artist(legend_trn)

        return self.canvas.draw()

    def plot_results(self, points, cls_real, prob_mesh, cls_diff, score=None, training=None):
        """
        Plots datasets, decision boundaries and classifications.
        :param ndarray points: Numpy array of points to plot
        :param cls_real: List or array of point classifications
        :type cls_real: list or ndarray
        :param tuple prob_mesh: Tuple of x,y point mesh and classification probabilities
        :param cls_diff: List or array of classification differences
        :type cls_diffl: list or ndarray
        :param float score: Classification precision score value
        """
        #define colormaps
        bound_cmap = matplotlib.cm.get_cmap('viridis')                              #decision boundary cmap
        cls_mark_cmap = matplotlib.colors.ListedColormap(['#FFFFFF', '#000000'])    #wrong classification cmap
        self.reset_canvas(prob_mesh)
        #plot decision boundary
        self.ax.contourf(prob_mesh[0], prob_mesh[1], prob_mesh[2], cmap=bound_cmap, alpha=.8)
        if score is not None:
            self.ax.text(0.95, 0.05, ('%.2f' % score).lstrip('0'), size=18, horizontalalignment='right', transform=self.ax.transAxes)
        if training is not None:
            self.plot_training(training[0], training[1])
        #plot dataset
        point_scatter = self.ax.scatter(points[:, 0], points[:, 1], c=cls_real, cmap=self.point_cmap, edgecolors='k')
        legend_pts = self.ax.legend(*point_scatter.legend_elements(), loc="lower left", title="Classifications")
        self.ax.add_artist(legend_pts)
        #plot incorrect classifications
        self.ax.scatter(points[:,0], points[:,1],c=cls_diff,cmap=cls_mark_cmap,marker='x',s=np.multiply(cls_diff,40))

        return self.canvas.draw()