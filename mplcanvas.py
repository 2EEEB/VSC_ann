from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg

class MplCanvas(FigureCanvasQTAgg):
    def __init__(self, parent=None):
        super(MplCanvas, self).__init__()